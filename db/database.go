package db

import (
	"errors"

	"github.com/glebarez/sqlite"
	"github.com/pquerna/otp/totp"
	"github.com/shair/entities"
	"github.com/shair/util"
	"gorm.io/gorm"
)

var Database *gorm.DB

func Init() {
	var err error
	Database, err = gorm.Open(sqlite.Open("data/db.sqlite"), &gorm.Config{})

	if err != nil {
		panic(err.Error())
	}

	Database.AutoMigrate(&entities.User{}, &entities.Note{}, &entities.Upload{}, &entities.Paste{}, &entities.Bookmark{}, &entities.Poll{}, &entities.CodeSnippet{})
}

func CreateUser(username string, password string) (entities.User, error) {
	user := entities.User{
		Username: username,
	}
	if Database.Where(&user).Find(&user).RowsAffected != 0 {
		return user, errors.New("Username already taken!")
	}

	user.Password = util.HashPassword(password)

	Database.Create(&user)
	return user, nil
}

func CreateOrGetUserFromOidc(username, subjectId string) entities.User {
	user := entities.User{
		OidcSubject: subjectId,
	}
	userExists := Database.Where(&user).Find(&user).RowsAffected != 0

	if !userExists || user.Username != username {
		// find a new unique username for the OIDC user
		for Database.Where(entities.User{Username: username}).Find(entities.User{}).RowsAffected != 0 || util.IsBlank(username) {
			username = username + "-" + util.GenerateSecureToken(10)
		}
		user.Username = username

		if !userExists {
			Database.Create(&user)
		} else {
			// update username if changed at OIDC Provider
			Database.Where(entities.User{OidcSubject: subjectId}).Updates(&user)
		}
	}

	return user
}

func LoginUser(username string, password string, otp string) (entities.User, error) {
	user := entities.User{
		Username: username,
	}

	if Database.Where(&user).Find(&user).RowsAffected == 0 {
		return user, errors.New("User not found!")
	}

	if !util.CheckPasswordHash(password, user.Password) {
		return user, errors.New("Invalid password!")
	}

	if !util.IsBlank(user.TotpSecret) && !totp.Validate(otp, user.TotpSecret) {
		return user, errors.New("Invalid Password!")
	}

	return user, nil
}

func FindUser(userId uint) (entities.User, error) {
	user := entities.User{
		Model: gorm.Model{ID: userId},
	}

	if Database.Where(&user).Find(&user).RowsAffected == 0 {
		return user, errors.New("User not found!")
	}

	return user, nil
}

func DeleteUser(userId uint) {
	Database.Delete(&entities.User{Model: gorm.Model{ID: userId}})

	query := Database.Where("userId = ?", userId)
	query.Delete(&entities.Note{})
	query.Delete(&entities.Bookmark{})
	query.Delete(&entities.CodeSnippet{})
}
