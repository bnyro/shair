package middleware

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/util"
)

const AuthCookie = "Authorization"
const AuthHeader = "Authorization"
const RequestUserKey = "userKey"

func ValidateAuthentication(publicPrefixes []string, publicPaths []string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			isRestrictedPath := true
			for _, publicPrefix := range publicPrefixes {
				if strings.HasPrefix(c.Path(), publicPrefix) {
					isRestrictedPath = false
					break
				}
			}
			for _, path := range publicPaths {
				if path == c.Path() {
					isRestrictedPath = false
					break
				}
			}

			user, err := GetUserByCookie(c)
			if err != nil && isRestrictedPath {
				path := url.QueryEscape(c.Path())
				return c.Redirect(http.StatusTemporaryRedirect, fmt.Sprintf("/auth/?redirect=%s", path))
			} else {
				c.Set(RequestUserKey, user)
			}

			return next(c)
		}
	}
}

func GetUserByCookie(c echo.Context) (entities.User, error) {
	authToken := ""
	if authCookie, err := c.Cookie(AuthCookie); err == nil {
		authToken = authCookie.Value
	} else {
		authToken = c.Request().Header.Get(AuthHeader)
		authToken = strings.TrimPrefix(authToken, "Token ")
		authToken = strings.TrimPrefix(authToken, "Bearer ")
	}

	if util.IsBlank(authToken) {
		return entities.User{}, errors.New("Auth token missing!")
	}

	token, err := jwt.ParseWithClaims(authToken, &entities.JwtClaim{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.ServerConfig.SecretKey), nil
	})
	if err != nil {
		return entities.User{}, err
	}

	t := token.Claims.(*entities.JwtClaim)
	user, err := db.FindUser(t.UserId)
	if err != nil {
		return entities.User{}, err
	}

	return user, nil
}

func GenerateJwtToken(userId uint, durationValid time.Duration) (string, error) {
	claims := &entities.JwtClaim{
		UserId: userId,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(durationValid)),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(config.ServerConfig.SecretKey))
	if err != nil {
		return "", err
	}

	return t, nil
}
