FROM golang:alpine AS build
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -o /app/shair

FROM alpine
WORKDIR /app

COPY --from=build "/app/shair" "/app/shair"
COPY --from=build "/app/static" "/app/static"
COPY --from=build "/app/templates" "/app/templates"
RUN mkdir /app/data

EXPOSE 3000
CMD ["/app/shair"]
