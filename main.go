package main

import (
	"fmt"
	"html/template"
	"io"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/time/rate"

	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/handlers"
	mw "github.com/shair/middleware"
)

type Template struct{}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	theme := "material"
	themeCookie, err := c.Cookie(handlers.ThemeCookie)
	if err == nil {
		theme = themeCookie.Value
	}

	tmpl, err := template.ParseFiles("templates/base.html", "templates/"+name)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	tmpl, _ = tmpl.Parse(fmt.Sprintf("{{ define \"theme\" }}class=\"%s\"{{ end }}", theme))

	err = tmpl.Execute(w, data)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

func main() {
	db.Init()

	config.Init()

	go runWorker()

	router := echo.New()
	router.Use(middleware.BodyLimit(fmt.Sprintf("%dM", config.ServerConfig.MaxFileSize)))
	router.Use(middleware.CORS())
	router.Use(mw.ValidateAuthentication([]string{"/api/v1/", "/code/view", "/code/raw", "/paste/view", "/upload/view", "/auth", "/static", "/captcha", "/oidc", "/qr", "/files"}, []string{"/"}))
	router.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))
	router.Renderer = &Template{}
	router.HTTPErrorHandler = handlers.CustomHTTPErrorHandler

	router.GET("/status", handlers.Status)
	router.Static("/static", "static")

	router.Any("/", handlers.Home)
	router.Any("/settings", handlers.Settings)
	router.POST("/settings/theme", handlers.SetTheme)
	router.GET("/qr", handlers.QrCode)

	auth := router.Group("auth")
	auth.Any("/", handlers.Auth)
	auth.GET("/captcha", handlers.GetCaptcha)

	// Paths that don't require to be authenticated yet, thus ratelimited
	ratelimited := auth.Group("")
	rateLimiterConfig := middleware.NewRateLimiterMemoryStore(rate.Limit(1))
	ratelimited.Use(middleware.RateLimiter(rateLimiterConfig))
	if config.ServerConfig.RegistrationEnabled {
		ratelimited.POST("/register", handlers.RegisterUser)
	}
	ratelimited.POST("/login", handlers.LoginUser)

	user := router.Group("/user")
	user.POST("/logout", handlers.LogoutUser)
	user.POST("/delete", handlers.DeleteUser)
	user.POST("/totp/validate", handlers.ValidateTotp)
	user.GET("/totp/qr", handlers.TotpQrCode)
	user.POST("/totp/disable", handlers.DisableTotp)

	if handlers.InitOIDC() {
		oidc := router.Group("/oidc")
		oidc.GET("/login", handlers.OIDCLogin)
		oidc.GET("/login/callback", handlers.OIDCLoginCallback)
		oidc.GET("/delete", handlers.OIDCDeleteLogin)
		oidc.GET("/delete/callback", handlers.OIDCDeleteCallback)
	}

	paste := router.Group("/paste")
	paste.GET("/", handlers.NewPastePage)
	paste.POST("/", handlers.CreateNewPaste)
	paste.GET("/view/:token", handlers.GetPaste)

	upload := router.Group("/upload")
	upload.GET("/", handlers.NewUploadPage)
	upload.POST("/", handlers.CreateNewUpload)
	upload.GET("/view/:token", handlers.GetUpload)
	router.Static("/files", config.ServerConfig.UploadDir)

	notes := router.Group("/notes")
	notes.Any("/", handlers.ListNotes)
	notes.POST("/new", handlers.NewNote)
	notes.POST("/pin/:id", handlers.PinNote)
	notes.POST("/update/:id", handlers.UpdateNote)
	notes.POST("/delete/:id", handlers.DeleteNote)

	bookmarks := router.Group("/bookmarks")
	bookmarks.Any("/", handlers.ListBookmarks)
	bookmarks.Any("/archived/", handlers.ListBookmarksArchived)
	bookmarks.POST("/new", handlers.NewBookmark)
	bookmarks.POST("/update/:id", handlers.UpdateBookmark)
	bookmarks.POST("/archive/:id", handlers.ArchiveBookmark)
	bookmarks.POST("/delete/:id", handlers.DeleteBookmark)

	codeSnippets := router.Group("/code")
	codeSnippets.Any("/", handlers.ListCodeSnippets)
	codeSnippets.POST("/new", handlers.NewCodeSnippet)
	codeSnippets.POST("/update/:id", handlers.UpdateCodeSnippet)
	codeSnippets.POST("/delete/:id", handlers.DeleteCodeSnippet)
	codeSnippets.Any("/view/:id", handlers.GetCodeSnippet)
	codeSnippets.GET("/raw/:id", handlers.RawCodeSnippet)

	poll := router.Group("/poll")
	poll.GET("/", handlers.CreateNewPoll)
	poll.POST("/new", handlers.CreateNewPoll)
	poll.GET("/:token", handlers.GetPoll)
	poll.POST("/:token", handlers.SubmitPollResponse)

	memosApi := router.Group("/api/v1")
	memosApi.POST("/auth/status", handlers.AuthStatus)
	memosApi.GET("/workspace/profile", handlers.WorkspaceProfile)
	memosApi.GET("/status", handlers.AuthStatus)
	memosApi.GET("/users/:name/setting", handlers.UserSetting)
	memosApi.GET("/memos", handlers.ListNotesApi)
	memosApi.POST("/memos", handlers.NewNoteApi)
	memosApi.PATCH("/memos/:id", handlers.UpdateNoteApi)
	memosApi.DELETE("/memos/:id", handlers.DeleteNoteApi)

	bookmarksApi := router.Group("/api/bookmarks")
	bookmarksApi.GET("/", handlers.ListBookmarksApi)
	bookmarksApi.GET("/archived/", handlers.ListBookmarksArchivedApi)
	bookmarksApi.POST("/", handlers.NewBookmarkApi)
	bookmarksApi.GET("/:id/", handlers.GetBookmarkApi)
	bookmarksApi.PUT("/:id/", handlers.UpdateBookmarkApi)
	bookmarksApi.POST("/:id/archive/", handlers.ArchiveBookmarkApi)
	bookmarksApi.POST("/:id/unarchive/", handlers.UnarchiveBookmarkApi)
	bookmarksApi.DELETE("/:id/", handlers.DeleteBookmarkApi)

	router.Logger.Fatal(router.Start(":3000"))
}
