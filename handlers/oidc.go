package handlers

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/util"
	"golang.org/x/oauth2"
)

const StateCookie = "state"
const ChallengeCookie = "challenge"

var oauth2Config *oauth2.Config
var provider *oidc.Provider
var verifier *oidc.IDTokenVerifier
var oidcContext = context.Background()

type OidcClaims struct {
	Email             string `json:"email"`
	PreferredUsername string `json:"preferred_username"`
}

func InitOIDC() bool {
	if !config.ServerConfig.OidcEnabled {
		return false
	}

	var err error
	provider, err = oidc.NewProvider(oidcContext, config.ServerConfig.Issuer)
	if err != nil {
		log.Println(err)
		return false
	}

	oauth2Config = &oauth2.Config{
		ClientID:     config.ServerConfig.ClientID,
		ClientSecret: config.ServerConfig.ClientSecret,
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
	verifier = provider.Verifier(&oidc.Config{ClientID: config.ServerConfig.ClientID})

	return true
}

func OIDCLogin(c echo.Context) error {
	authCodeUrl := genAuthCodeUrl(c, "/oidc/login/callback")

	return c.Redirect(http.StatusFound, authCodeUrl)
}

func OIDCLoginCallback(c echo.Context) error {
	user, err := authenticateOidcUser(c)
	if err != nil {
		return err
	}

	SetAuthCookie(c, &user)
	return c.Redirect(http.StatusTemporaryRedirect, "/")
}

func OIDCDeleteLogin(c echo.Context) error {
	authCodeUrl := genAuthCodeUrl(c, "/oidc/delete/callback")

	return c.Redirect(http.StatusFound, authCodeUrl)
}

func OIDCDeleteCallback(c echo.Context) error {
	user, err := authenticateOidcUser(c)
	if err != nil {
		return err
	}

	db.DeleteUser(user.ID)
	SetAuthCookie(c, nil)
	return c.Redirect(http.StatusTemporaryRedirect, "/")
}

func genAuthCodeUrl(c echo.Context, path string) string {
	state := util.GenerateSecureToken(20)
	verifierRand := oauth2.GenerateVerifier()

	util.SetCookieSecureWithMaxAge(c, StateCookie, state, int(time.Hour.Seconds()))
	util.SetCookieSecureWithMaxAge(c, ChallengeCookie, verifierRand, int(time.Hour.Seconds()))

	// copy the OAuth2 config with a different redirect url
	reqConfig := *oauth2Config
	reqConfig.RedirectURL = util.GetUrlForPath(c, path).String()

	challengeVerifier := oauth2.S256ChallengeOption(verifierRand)
	return reqConfig.AuthCodeURL(state, challengeVerifier)
}

func authenticateOidcUser(c echo.Context) (entities.User, error) {
	state, err := c.Cookie("state")
	if err != nil {
		return entities.User{}, echo.ErrBadRequest
	}

	challenge, err := c.Cookie("challenge")
	if err != nil {
		return entities.User{}, echo.ErrBadRequest
	}

	util.SetCookieSecure(c, StateCookie, "")
	util.SetCookieSecure(c, ChallengeCookie, "")

	if c.QueryParam("state") != state.Value {
		return entities.User{}, echo.ErrBadRequest
	}

	verifierOption := oauth2.VerifierOption(challenge.Value)
	oauth2Token, err := oauth2Config.Exchange(oidcContext, c.QueryParam("code"), verifierOption)
	if err != nil {
		return entities.User{}, err
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		return entities.User{}, errors.New("failed to extract id")
	}

	idToken, err := verifier.Verify(oidcContext, rawIDToken)
	if err != nil {
		return entities.User{}, echo.ErrBadRequest
	}

	var claims OidcClaims
	if err := idToken.Claims(&claims); err != nil {
		return entities.User{}, err
	}

	username := claims.Email
	if !util.IsBlank(claims.PreferredUsername) {
		username = claims.PreferredUsername
	}

	return db.CreateOrGetUserFromOidc(username, idToken.Subject), nil
}
