package handlers

import (
	"image/png"
	"net/http"
	"net/url"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/middleware"
	"github.com/shair/util"
)

const ThemeCookie = "Theme"

func Settings(c echo.Context) error {
	user, userErr := middleware.GetUserByCookie(c)
	isOidc := userErr == nil && !util.IsBlank(user.OidcSubject)

	data := echo.Map{
		"LoggedIn": userErr == nil,
		"OIDC":     isOidc,
	}

	if userErr == nil {
		authToken, _ := middleware.GenerateJwtToken(user.ID, 365*24*time.Hour) // token expires after one year
		data["AuthToken"] = authToken
	}

	if userErr == nil && !isOidc {
		totpSecret, err := totp.Generate(totp.GenerateOpts{
			Issuer:      "Shair",
			AccountName: user.Username,
		})

		if err != nil {
			return err
		}

		data["TOTPSecret"] = totpSecret.Secret()
		data["TOTPQrCodeUrl"] = url.QueryEscape(totpSecret.URL())
	}

	return c.Render(http.StatusOK, "settings.html", data)
}

func SetTheme(c echo.Context) error {
	theme := c.FormValue("theme")
	util.SetCookieSecure(c, ThemeCookie, theme)

	return c.Redirect(http.StatusTemporaryRedirect, "/settings")
}

func ValidateTotp(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	secret := c.FormValue("secret")

	validation, _ := url.QueryUnescape(c.FormValue("validation"))

	if !totp.Validate(validation, secret) {
		return echo.ErrBadRequest
	}

	user.TotpSecret = secret
	db.Database.Where("id = ?", user.ID).Updates(&user)

	return c.Redirect(http.StatusTemporaryRedirect, "/settings")
}

func TotpQrCode(c echo.Context) error {
	otpauthUrl, err := url.QueryUnescape(c.QueryParam("url"))
	if err != nil {
		return err
	}

	key, err := otp.NewKeyFromURL(otpauthUrl)
	if err != nil {
		return err
	}

	image, err := key.Image(300, 300)
	if err != nil {
		return err
	}

	return png.Encode(c.Response().Writer, image)
}

func DisableTotp(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	db.Database.Model(&user).Where("id = ?", user.ID).Update("totp_secret", "")

	return c.Redirect(http.StatusTemporaryRedirect, "/settings")
}
