package handlers

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

func CustomHTTPErrorHandler(err error, c echo.Context) {
	if c.Response().Committed {
		return
	}

	if strings.HasPrefix(c.Request().URL.Path, "/api") {
		c.Echo().DefaultHTTPErrorHandler(err, c)
		return
	}

	code := http.StatusInternalServerError
	message := err.Error()
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		message = fmt.Sprint(he.Message)
	}
	c.Render(code, "error.html", echo.Map{
		"Code":  code,
		"Error": message,
	})
}
