package handlers

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/extractors"
	"github.com/shair/middleware"
	"github.com/shair/util"
	"gorm.io/gorm"
)

type BookmarkResp struct {
	Id              uint   `json:"id"`
	Url             string `json:"url"`
	Title           string `json:"title"`
	Description     string `json:"description"`
	PreviewImageUrl string `json:"preview_image_url"`
	DateAdded       string `json:"date_added"`
	DateModified    string `json:"date_modified"`
	IsArchived      bool   `json:"is_archived"`
	Unread          bool   `json:"unread"`
	Shared          bool   `json:"shared"`
}

type PaginatedResp struct {
	Count    int            `json:"count"`
	Next     string         `json:"next"`
	Previous string         `json:"previous"`
	Results  []BookmarkResp `json:"results"`
}

var client = http.Client{
	Timeout: time.Duration(config.ServerConfig.HttpTimeoutMs) * time.Millisecond,
	CheckRedirect: func(req *http.Request, via []*http.Request) error {
		if !util.ValidateHost(req.Host) {
			return errors.New("Can't resolve redirect.")
		}

		return nil
	},
}

func ListBookmarks(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	archived := false
	bookmarks := getBookmarks(user, &archived)

	return c.Render(http.StatusOK, "bookmarks.html", echo.Map{
		"Archived":  archived,
		"Bookmarks": bookmarks,
	})
}

func ListBookmarksArchived(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	archived := true
	bookmarks := getBookmarks(user, &archived)

	return c.Render(http.StatusOK, "bookmarks.html", echo.Map{
		"Archived":  archived,
		"Bookmarks": bookmarks,
	})
}

func NewBookmark(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	var bookmark entities.Bookmark
	if err := c.Bind(&bookmark); err != nil {
		return echo.ErrBadRequest
	}

	bookmark, err := fetchBookmarkDetails(bookmark)
	if err != nil {
		return err
	}

	bookmark.UserID = user.ID
	db.Database.Create(&bookmark)

	return c.Redirect(301, "/bookmarks/")
}

func UpdateBookmark(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	searchBookmark := entities.Bookmark{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	var bookmark entities.Bookmark
	if err = c.Bind(&bookmark); err != nil {
		return echo.ErrBadRequest
	}

	if db.Database.Where(&searchBookmark).Updates(&bookmark).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/bookmarks/")
}

func DeleteBookmark(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return echo.ErrBadRequest
	}

	if !deleteBookmark(user, id) {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/bookmarks/")
}

func ArchiveBookmark(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	bookmark := getBookmark(user, id)
	if bookmark == nil {
		return echo.ErrBadRequest
	}

	if err := archiveBookmark(c, !bookmark.Archived); err != nil {
		return err
	}

	if bookmark.Archived {
		return c.Redirect(http.StatusTemporaryRedirect, "/bookmarks/archived/")
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, "/bookmarks/")
	}
}

func archiveBookmark(c echo.Context, archive bool) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	searchBookmark := entities.Bookmark{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	bookmark := entities.Bookmark{
		Archived: archive,
	}
	if db.Database.Where(&searchBookmark).Select("archived").Updates(bookmark).RowsAffected == 0 {
		return echo.ErrBadRequest
	}
	return nil
}

func ListBookmarksApi(c echo.Context) error {
	return listBookmarksApi(c, nil)
}

func ListBookmarksArchivedApi(c echo.Context) error {
	archivedOnly := true
	return listBookmarksApi(c, &archivedOnly)
}

func GetBookmarkApi(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return echo.ErrBadRequest
	}

	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	bookmark := getBookmark(user, id)
	if bookmark == nil {
		return echo.ErrBadRequest
	}

	return c.JSON(http.StatusOK, toBookmarkResp(*bookmark))
}

func NewBookmarkApi(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	bookmark := entities.Bookmark{}
	if err := c.Bind(&bookmark); err != nil {
		return echo.ErrBadRequest
	}

	bookmark, err := fetchBookmarkDetails(bookmark)
	if err != nil {
		return err
	}

	bookmark.UserID = user.ID
	db.Database.Create(&bookmark)

	return c.JSON(http.StatusCreated, toBookmarkResp(bookmark))
}

func DeleteBookmarkApi(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	if !deleteBookmark(user, id) {
		return echo.ErrNotFound
	}

	return c.NoContent(http.StatusOK)
}

func UpdateBookmarkApi(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	var bookmarkResp BookmarkResp
	if err := c.Bind(&bookmarkResp); err != nil {
		return echo.ErrBadRequest
	}

	searchBookmark := entities.Bookmark{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	oldBookmark := getBookmark(user, id)
	if oldBookmark == nil {
		return echo.ErrBadRequest
	}

	bookmark := entities.Bookmark{
		Url:         util.IfBlank(bookmarkResp.Url, oldBookmark.Url),
		Title:       util.IfBlank(bookmarkResp.Title, oldBookmark.Title),
		Description: util.IfBlank(bookmarkResp.Description, oldBookmark.Description),
		Thumbnail:   util.IfBlank(bookmarkResp.PreviewImageUrl, oldBookmark.Thumbnail),
		Archived:    !bookmarkResp.Unread,
	}

	if db.Database.Where(&searchBookmark).Updates(&bookmark).RowsAffected == 0 {
		return echo.ErrNotFound
	}
	db.Database.Where(&searchBookmark).Select("archived").Updates(&bookmark)

	return c.JSON(http.StatusOK, toBookmarkResp(*getBookmark(user, id)))
}

func ArchiveBookmarkApi(c echo.Context) error {
	if err := archiveBookmark(c, true); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func UnarchiveBookmarkApi(c echo.Context) error {
	if err := archiveBookmark(c, false); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func getBookmarks(user entities.User, archived *bool) []entities.Bookmark {
	exampleBookmark := entities.Bookmark{
		UserID: user.ID,
	}

	var bookmarks []entities.Bookmark
	query := db.Database.Where(exampleBookmark)
	if archived != nil && *archived == true {
		query = query.Where("archived = ?", true)
	} else if archived != nil && *archived == false {
		query = query.Where("archived = false OR archived IS NULL")
	}
	query.Find(&bookmarks)

	return bookmarks
}

func getBookmark(user entities.User, id int) *entities.Bookmark {
	bookmark := entities.Bookmark{
		UserID: user.ID,
		Model: gorm.Model{
			ID: uint(id),
		},
	}
	if db.Database.Where(bookmark).First(&bookmark).RowsAffected == 0 {
		return nil
	}
	return &bookmark
}

func deleteBookmark(user entities.User, bookmarkID int) bool {
	if bookmarkID == 0 {
		return false
	}

	bookmark := entities.Bookmark{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(bookmarkID)},
	}

	return db.Database.Where(&bookmark).Delete(&bookmark).RowsAffected == 1
}

func toBookmarkResp(bookmark entities.Bookmark) BookmarkResp {
	return BookmarkResp{
		Id:              bookmark.ID,
		Url:             bookmark.Url,
		Title:           bookmark.Title,
		Description:     bookmark.Description,
		PreviewImageUrl: bookmark.Thumbnail,
		DateAdded:       bookmark.Model.CreatedAt.Format(time.RFC3339Nano),
		DateModified:    bookmark.Model.UpdatedAt.Format(time.RFC3339Nano),
		IsArchived:      bookmark.Archived,
		Unread:          !bookmark.Archived,
		Shared:          bookmark.Archived,
	}
}

func listBookmarksApi(c echo.Context, archived *bool) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	bookmarks := getBookmarks(user, archived)

	var respBookmarks []BookmarkResp
	for _, bookmark := range bookmarks {
		respBookmarks = append(respBookmarks, toBookmarkResp(bookmark))
	}

	return c.JSON(http.StatusOK, PaginatedResp{
		Count:   len(bookmarks),
		Results: respBookmarks,
	})
}

func fetchBookmarkDetails(bookmark entities.Bookmark) (entities.Bookmark, error) {
	parsedUrl, err := url.Parse(bookmark.Url)

	if err != nil {
		return bookmark, echo.ErrBadRequest
	}

	if util.ValidateHost(parsedUrl.Host) {
		if resp, err := client.Get(bookmark.Url); err == nil {
			defer resp.Body.Close()

			site := extractors.Extract(resp.Body)
			if util.IsBlank(bookmark.Title) {
				bookmark.Title = site.Title
			}

			if util.IsBlank(bookmark.Description) {
				bookmark.Description = site.Description
			}

			bookmark.Thumbnail = site.Image
		}
	}

	if util.IsBlank(bookmark.Title) {
		bookmark.Title = parsedUrl.Host
	}

	return bookmark, nil
}
