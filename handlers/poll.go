package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/lib/pq"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/util"
)

const maxAmountOfChoices = 10

func CreateNewPoll(c echo.Context) error {
	if c.Request().Method == "GET" {
		choicesIter := util.MakeIntArray(maxAmountOfChoices)

		return c.Render(http.StatusOK, "newpoll.html", echo.Map{
			"ChoicesIter": choicesIter,
		})
	}

	var poll entities.Poll
	if err := c.Bind(&poll); err != nil {
		return echo.ErrBadRequest
	}

	var choices pq.StringArray
	var votes pq.Int64Array

	for choicesIndex := 0; choicesIndex < maxAmountOfChoices; choicesIndex++ {
		choice := c.FormValue(fmt.Sprintf("choice_%d", choicesIndex))
		if !util.IsBlank(choice) {
			choices = append(choices, choice)
			votes = append(votes, 0)
		}
	}

	if len(choices) == 0 {
		return echo.ErrBadRequest
	}

	poll.Choices = choices
	poll.Votes = votes
	poll.Token = util.GenerateSecureToken(20)
	db.Database.Create(&poll)

	return util.CreateSuccessResult(c, "poll", poll.Token)
}

func GetPoll(c echo.Context) error {
	token := c.Param("token")
	poll := entities.Poll{
		Token: token,
	}

	if db.Database.Where(&poll).Find(&poll).RowsAffected == 0 {
		return echo.ErrNotFound
	}

	voted, cookieErr := c.Cookie(token)
	return c.Render(http.StatusOK, "poll.html", echo.Map{
		"Poll":         poll,
		"AlreadyVoted": cookieErr == nil && !util.IsBlank(voted.Value),
	})
}

func SubmitPollResponse(c echo.Context) error {
	selection, err := strconv.Atoi(c.FormValue("selection"))
	if err != nil {
		return err
	}

	token := c.Param("token")
	if voted, cookieErr := c.Cookie(token); cookieErr == nil && !util.IsBlank(voted.Value) {
		return echo.ErrForbidden
	}

	poll := entities.Poll{
		Token: token,
	}

	if db.Database.Where(&poll).Find(&poll).RowsAffected == 0 {
		return echo.ErrNotFound
	}

	poll.Votes[selection]++
	db.Database.Updates(&poll)

	util.SetCookieSecureWithPath(c, token, "1", "/poll")

	return c.Render(http.StatusOK, "poll.html", echo.Map{
		"Poll":         poll,
		"AlreadyVoted": true,
	})
}
