package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/shair/middleware"
)

func Home(c echo.Context) error {
	_, userErr := middleware.GetUserByCookie(c)

	return c.Render(http.StatusOK, "home.html", echo.Map{
		"LoggedIn": userErr == nil,
	})
}

func Status(c echo.Context) error {
	return c.JSON(http.StatusOK, echo.Map{
		"message": "ok",
	})
}
