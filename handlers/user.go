package handlers

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/mojocn/base64Captcha"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/middleware"
	"github.com/shair/util"
)

const CaptchaIdCookie = "CaptchaId"

func Auth(c echo.Context) error {
	return c.Render(http.StatusOK, "auth.html", echo.Map{
		"Redirect":            c.QueryParam("redirect"),
		"OIDCAvailable":       config.ServerConfig.OidcEnabled,
		"RegistrationEnabled": config.ServerConfig.RegistrationEnabled,
	})
}

func GetCaptcha(c echo.Context) error {
	captcha := base64Captcha.NewCaptcha(base64Captcha.DefaultDriverDigit, base64Captcha.DefaultMemStore)
	id, b64s, _, _ := captcha.Generate()

	c.Response().Header().Add("Content-Type", "image/png")
	util.SetCookieSecure(c, CaptchaIdCookie, id)

	img, err := base64.StdEncoding.DecodeString(strings.Split(b64s, ",")[1])
	if err != nil {
		fmt.Println(err)
		return err
	}

	_, err = c.Response().Writer.Write(img)
	return err
}

func RegisterUser(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	captchaResp := c.FormValue("captcha")
	captchaId, err := c.Cookie(CaptchaIdCookie)
	redirect := c.FormValue("redirect")

	if util.IsBlank(username) || util.IsBlank(password) || err != nil {
		return echo.ErrBadRequest
	}
	if !base64Captcha.DefaultMemStore.Verify(captchaId.Value, captchaResp, true) {
		return c.Redirect(http.StatusTemporaryRedirect, "/auth/")
	}

	user, err := db.CreateUser(username, password)
	if err != nil {
		return echo.ErrConflict
	}

	util.SetCookieSecure(c, CaptchaIdCookie, "/")
	SetAuthCookie(c, &user)

	if !strings.HasPrefix(redirect, "/") {
		// make sure to not redirect to external websites
		redirect = "/"
	}

	return c.Redirect(http.StatusTemporaryRedirect, redirect)
}

func LoginUser(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	totp := c.FormValue("totp")
	redirect := c.FormValue("redirect")
	if !strings.HasPrefix(redirect, "/") {
		// make sure to not redirect to external websites
		redirect = "/"
	}

	user, err := db.LoginUser(username, password, totp)
	if err != nil {
		path := url.QueryEscape(redirect)
		return c.Redirect(http.StatusTemporaryRedirect, fmt.Sprintf("/auth/?redirect=%s", path))
	}

	SetAuthCookie(c, &user)

	return c.Redirect(http.StatusTemporaryRedirect, redirect)
}

func DeleteUser(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	password := c.FormValue("password")

	if !util.CheckPasswordHash(password, user.Password) {
		return echo.ErrUnauthorized
	}

	db.DeleteUser(user.ID)

	return LogoutUser(c)
}

func LogoutUser(c echo.Context) error {
	SetAuthCookie(c, nil)

	return c.Redirect(http.StatusTemporaryRedirect, "/")
}

func SetAuthCookie(c echo.Context, user *entities.User) {
	token := ""

	if user != nil {
		var err error
		token, err = middleware.GenerateJwtToken(user.ID, 7*24*time.Hour)
		if err != nil {
			return
		}
	}

	util.SetCookieSecure(c, middleware.AuthCookie, token)
}
