package handlers

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/structs"
	"github.com/labstack/echo/v4"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/middleware"
	"github.com/shair/util"
	"gorm.io/gorm"
)

type NoteResp struct {
	Name        string   `json:"name"`
	UID         string   `json:"uid"`
	RowStatus   string   `json:"rowStatus"`
	Creator     string   `json:"creator"`
	CreateTime  string   `json:"createTime"`
	UpdateTime  string   `json:"updateTime"`
	DisplayTime string   `json:"displayTime"`
	Content     string   `json:"content"`
	Visiblity   string   `json:"visibility"`
	Tags        []string `json:"tags"`
	Pinned      bool     `json:"pinned"`
	Resources   []string `json:"resources"`
}

type PaginatedNoteResp struct {
	Memos         []NoteResp `json:"memos"`
	NextPageToken string     `json:"nextPageToken"`
}

func ListNotes(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	exampleNote := entities.Note{
		UserID: user.ID,
	}

	var notes []entities.Note
	db.Database.Where(&exampleNote).Order("pinned desc").Find(&notes)
	var notesMap []map[string]interface{}
	for _, note := range notes {
		noteStr := structs.Map(note)
		noteStr["Formatted"] = template.HTML(util.Urlify(note.Body))
		noteStr["ID"] = note.ID
		noteStr["CreatedAt"] = note.CreatedAt
		noteStr["UpdatedAt"] = note.UpdatedAt
		notesMap = append(notesMap, noteStr)
	}

	return c.Render(http.StatusOK, "notes.html", notesMap)
}

func ListNotesApi(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	exampleNote := entities.Note{
		UserID: user.ID,
	}

	var notes []entities.Note
	db.Database.Where(&exampleNote).Order("pinned desc").Find(&notes)

	var respNotes []NoteResp
	for _, note := range notes {
		respNotes = append(respNotes, toNoteResp(note, user))
	}

	return c.JSON(http.StatusOK, PaginatedNoteResp{
		Memos: respNotes,
	})
}

func NewNote(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	var note entities.Note
	if err := c.Bind(&note); err != nil {
		return echo.ErrBadRequest
	}
	note.UserID = user.ID
	db.Database.Create(&note)

	return c.Redirect(http.StatusTemporaryRedirect, "/notes/")
}

func NewNoteApi(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	var noteResp NoteResp
	if err := c.Bind(&noteResp); err != nil || util.IsBlank(noteResp.Content) {
		return echo.ErrBadRequest
	}

	title, body := extractBodyAndTitle(noteResp.Content)

	note := entities.Note{
		Title:  title,
		Body:   body,
		Pinned: noteResp.Pinned,
		UserID: user.ID,
	}
	db.Database.Create(&note)

	return c.JSON(http.StatusOK, toNoteResp(note, user))
}

func PinNote(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	searchNote := entities.Note{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	var note entities.Note
	if err = db.Database.Where(&searchNote).First(&note).Error; err != nil {
		return echo.ErrNotFound
	}
	note.Pinned = !note.Pinned
	db.Database.Where(&searchNote).Select("pinned").Updates(&note)

	return c.Redirect(http.StatusTemporaryRedirect, "/notes/")
}

func UpdateNote(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	searchNote := entities.Note{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	var note entities.Note
	if err := c.Bind(&note); err != nil {
		return echo.ErrBadRequest
	}

	if db.Database.Where(&searchNote).Updates(&note).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/notes/")
}

func UpdateNoteApi(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return echo.ErrBadRequest
	}

	var noteResp NoteResp
	if err := c.Bind(&noteResp); err != nil {
		return echo.ErrBadRequest
	}
	searchNote := entities.Note{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}

	if !util.IsBlank(noteResp.Content) {
		title, body := extractBodyAndTitle(noteResp.Content)
		note := entities.Note{
			Title: title,
			Body:  body,
		}

		if db.Database.Where(&searchNote).Updates(&note).RowsAffected == 0 {
			return echo.ErrBadRequest
		}

		return c.JSON(http.StatusOK, toNoteResp(note, user))
	} else {
		note := entities.Note{
			Pinned: noteResp.Pinned,
		}
		// boolean values are only set back to false if we explicitely select them
		if db.Database.Model(&searchNote).Where(&searchNote).Select("Pinned").Updates(&note).RowsAffected == 0 {
			return echo.ErrBadRequest
		}
		db.Database.Where(&searchNote).First(&searchNote)

		return c.JSON(http.StatusOK, toNoteResp(searchNote, user))
	}
}

func DeleteNote(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil || id <= 0 {
		return err
	}

	note := entities.Note{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}

	if db.Database.Where(&note).Delete(&note).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/notes/")
}

func DeleteNoteApi(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil || id <= 0 {
		return echo.ErrBadRequest
	}

	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	note := entities.Note{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}

	if db.Database.Where(&note).Delete(&note).RowsAffected == 0 {
		return echo.ErrNotFound
	}

	return c.NoContent(http.StatusOK)
}

func toNoteResp(note entities.Note, user entities.User) NoteResp {
	content := note.Body
	if !util.IsBlank(note.Title) {
		content = fmt.Sprintf("%s\n\n%s", note.Title, note.Body)
	}

	return NoteResp{
		Name:        strconv.Itoa(int(note.ID)),
		UID:         strconv.Itoa(int(note.ID)),
		Creator:     user.Username,
		RowStatus:   "ACTIVE",
		Visiblity:   "PRIVATE",
		Content:     content,
		Pinned:      note.Pinned,
		CreateTime:  note.Model.CreatedAt.Format(time.RFC3339),
		UpdateTime:  note.Model.UpdatedAt.Format(time.RFC3339),
		DisplayTime: note.Model.UpdatedAt.Format(time.RFC3339),
		Tags:        []string{},
		Resources:   []string{},
	}
}

func extractBodyAndTitle(content string) (title, body string) {
	if strings.Contains(body, "\n\n") {
		parts := strings.SplitN(body, "\n\n", 2)
		return parts[0], parts[1]
	}
	return "", content
}
