package handlers

import (
	"net/http"
	"os"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/util"
)

func NewUploadPage(c echo.Context) error {
	return c.Render(http.StatusOK, "newupload.html", nil)
}

func CreateNewUpload(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}

	token := util.GenerateSecureToken(30)

	if err = util.Save(file, config.ServerConfig.UploadDir, token); err != nil {
		return err
	}

	now := time.Now().Unix()
	upload := entities.Upload{
		Name:    file.Filename,
		Size:    file.Size,
		Created: now,
		Expires: now + config.ServerConfig.TempLifeTime,
		Token:   token,
	}

	db.Database.Create(&upload)

	return util.CreateSuccessResultWithDownloadPossible(c, "upload/view", upload.Token)
}

func GetUpload(c echo.Context) error {
	upload := entities.Upload{
		Token: c.Param("token"),
	}

	if db.Database.Where(&upload).Find(&upload).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	data := echo.Map{
		"Upload": upload,
		"Size":   humanize.Bytes(uint64(upload.Size)),
	}

	return c.Render(http.StatusOK, "upload.html", data)
}

func DeleteExpiredUploads() {
	var uploads []entities.Upload

	currentTime := time.Now().Unix()
	query := db.Database.Model(&entities.Upload{}).Where("expires < ?", currentTime)

	query.Find(&uploads)

	for _, upload := range uploads {
		os.Remove(config.ServerConfig.UploadDir + "/" + upload.Token)
	}

	query.Delete(&uploads)
}
