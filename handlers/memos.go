package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/shair/entities"
	"github.com/shair/middleware"
)

func WorkspaceProfile(c echo.Context) error {
	return c.JSONBlob(200, []byte("{\"owner\":\"\", \"version\":\"0.22.5\", \"mode\":\"prod\", \"instanceUrl\":\"\"}"))
}

func MemosStatus(c echo.Context) error {
	return c.JSONBlob(200, []byte("{\"code\":5, \"message\":\"Not Found\", \"details\":[]}"))
}

func AuthStatus(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	return c.JSON(http.StatusOK, echo.Map{
		"name":        user.Username,
		"id":          user.ID,
		"role":        "USER",
		"rowStatus":   "NORMAL",
		"email":       user.Username,
		"nickname":    user.Username,
		"username":    user.Username,
		"description": "",
		"avatarUrl":   "",
		"createTime":  user.CreatedAt,
		"updateTime":  user.UpdatedAt,
	})
}

func UserSetting(c echo.Context) error {
	return c.JSONBlob(http.StatusOK, []byte("{\"name\":\"\", \"locale\":\"en\", \"appearance\":\"system\", \"memoVisibility\":\"PRIVATE\"}"))
}
