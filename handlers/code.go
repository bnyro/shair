package handlers

import (
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/fatih/structs"
	"github.com/go-enry/go-enry/v2"
	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/middleware"
	"github.com/shair/util"
	"gorm.io/gorm"
)

func ListCodeSnippets(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)
	exampleCodeSnippet := entities.CodeSnippet{
		UserID: user.ID,
	}

	var codeSnippets []entities.CodeSnippet
	db.Database.Where(&exampleCodeSnippet).Find(&codeSnippets)
	var codeSnippetsMap []map[string]interface{}
	for _, codeSnippet := range codeSnippets {
		codeSnippetsMap = append(codeSnippetsMap, getFormattedSnippet(codeSnippet))
	}

	return c.Render(http.StatusOK, "code.html", echo.Map{
		"Languages": util.HighlightLanguages(),
		"Snippets":  codeSnippetsMap,
	})
}

func GetCodeSnippet(c echo.Context) error {
	user, userErr := middleware.GetUserByCookie(c)

	userOrNil := &user
	if userErr != nil {
		userOrNil = nil
	}

	token, _ := url.QueryUnescape(c.QueryParam("token"))
	codeSnippet, err := getSnippetByID(userOrNil, c.Param("id"), token)
	if err != nil {
		return err
	}

	return c.Render(http.StatusOK, "codesnippet.html", getFormattedSnippet(codeSnippet))
}

func RawCodeSnippet(c echo.Context) error {
	user, userErr := middleware.GetUserByCookie(c)

	userOrNil := &user
	if userErr != nil {
		userOrNil = nil
	}
	token, _ := url.QueryUnescape(c.QueryParam("token"))
	codeSnippet, err := getSnippetByID(userOrNil, c.Param("id"), token)
	if err != nil {
		return err
	}

	return c.String(http.StatusOK, codeSnippet.Raw)
}

func NewCodeSnippet(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	snippet := extractSnippet(c)
	snippet.UserID = user.ID
	db.Database.Create(&snippet)

	return c.Redirect(http.StatusTemporaryRedirect, "/code/")
}

func UpdateCodeSnippet(c echo.Context) error {
	var user entities.User = c.Get(middleware.RequestUserKey).(entities.User)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return echo.ErrBadRequest
	}

	searchSnippet := entities.CodeSnippet{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}
	snippet := extractSnippet(c)
	if db.Database.Where(&searchSnippet).Updates(snippet).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/code/")
}

func DeleteCodeSnippet(c echo.Context) error {
	user := c.Get(middleware.RequestUserKey).(entities.User)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil || id <= 0 {
		return echo.ErrBadRequest
	}

	snippet := entities.CodeSnippet{
		UserID: user.ID,
		Model:  gorm.Model{ID: uint(id)},
	}

	if db.Database.Where(&snippet).Delete(&snippet).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	return c.Redirect(http.StatusTemporaryRedirect, "/code/")
}

func extractSnippet(c echo.Context) entities.CodeSnippet {
	var snippet entities.CodeSnippet

	if err := c.Bind(&snippet); err != nil {
		return snippet
	}

	if util.IsBlank(snippet.Lang) {
		snippet.Lang = enry.GetLanguage("", []byte(snippet.Raw))
	}
	snippet.Lang = strings.ToLower(snippet.Lang)

	return snippet
}

func getSnippetByID(userOrNil *entities.User, idString, token string) (entities.CodeSnippet, error) {
	id, err := strconv.Atoi(idString)
	if err != nil {
		return entities.CodeSnippet{}, echo.ErrBadRequest
	}

	exampleCodeSnippet := entities.CodeSnippet{
		Model: gorm.Model{ID: uint(id)},
	}

	var codeSnippet entities.CodeSnippet
	db.Database.Where(&exampleCodeSnippet).First(&codeSnippet)

	if userOrNil != nil {
		if userOrNil.ID != codeSnippet.UserID {
			return entities.CodeSnippet{}, echo.ErrForbidden
		}
	} else if getSnippetHash(id) != token {
		return entities.CodeSnippet{}, echo.ErrForbidden
	}

	return codeSnippet, nil
}

func getSnippetHash(id int) string {
	return util.HashMd5(config.ServerConfig.SecretKey + strconv.Itoa(id))
}

func getFormattedSnippet(codeSnippet entities.CodeSnippet) echo.Map {
	codeSnippetStr := structs.Map(codeSnippet)

	codeSnippetStr["Formatted"] = template.HTML(util.CodeHighlight(codeSnippet.Raw, codeSnippet.Lang))
	codeSnippetStr["ID"] = codeSnippet.ID
	codeSnippetStr["CreatedAt"] = codeSnippet.CreatedAt
	codeSnippetStr["UpdatedAt"] = codeSnippet.UpdatedAt
	codeSnippetStr["Token"] = url.QueryEscape(getSnippetHash(int(codeSnippet.ID)))

	return codeSnippetStr
}
