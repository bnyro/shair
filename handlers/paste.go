package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/shair/config"
	"github.com/shair/db"
	"github.com/shair/entities"
	"github.com/shair/util"
)

func NewPastePage(c echo.Context) error {
	return c.Render(http.StatusOK, "newpaste.html", nil)
}

func CreateNewPaste(c echo.Context) error {
	title := c.FormValue("title")
	body := c.FormValue("body")

	var cipherKey string

	if c.FormValue("encrypt") == "true" || c.FormValue("encrypt") == "on" {
		cipherKey = util.GenerateSecureToken(16)

		titleBytes, err := util.EncryptText([]byte(cipherKey), []byte(title))
		if err != nil {
			fmt.Println(err)
			return echo.ErrInternalServerError
		}
		bodyBytes, err := util.EncryptText([]byte(cipherKey), []byte(body))
		if err != nil {
			return echo.ErrInternalServerError
		}

		title = string(titleBytes)
		body = string(bodyBytes)
	}

	now := time.Now().Unix()
	paste := entities.Paste{
		Title:   title,
		Body:    body,
		Created: now,
		Expires: now + config.ServerConfig.TempLifeTime,
		Token:   util.GenerateSecureToken(30),
	}

	db.Database.Create(&paste)

	return util.CreateSuccessResultWithExtra(c, "paste/view", paste.Token, map[string]string{"enckey": cipherKey})
}

func GetPaste(c echo.Context) error {
	paste := entities.Paste{
		Token: c.Param("token"),
	}

	if db.Database.Where(&paste).Find(&paste).RowsAffected == 0 {
		return echo.ErrBadRequest
	}

	cipherKey := c.QueryParam("enckey")
	if !util.IsBlank(cipherKey) {
		titleBytes, err := util.DecryptText([]byte(cipherKey), []byte(paste.Title))
		if err != nil {
			return echo.ErrForbidden
		}
		bodyBytes, err := util.DecryptText([]byte(cipherKey), []byte(paste.Body))
		if err != nil {
			return echo.ErrForbidden
		}

		paste.Title = string(titleBytes)
		paste.Body = string(bodyBytes)
	}

	return c.Render(http.StatusOK, "paste.html", paste)
}

func DeleteExpiredPastes() {
	currentTime := time.Now().Unix()

	query := db.Database.Model(&entities.Paste{}).Where("expires < ?", currentTime)
	query.Delete(&entities.Paste{})
}
