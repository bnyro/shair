package config

import (
	"context"
	"log"
	"os"

	"github.com/sethvargo/go-envconfig"
	"github.com/shair/util"
)

type OidcConfig struct {
	ClientID     string `env:"OIDC_CLIENTID"`
	ClientSecret string `env:"OIDC_CLIENTSECRET"`
	Issuer       string `env:"OIDC_ISSUER"`
}

type Config struct {
	SecretKey           string `env:"SECRETKEY, default="`
	RegistrationEnabled bool   `env:"REGISTRATION, default=true"`

	OidcConfig
	OidcEnabled bool

	TempLifeTime  int64  `env:"LIFETIME, default=604800"` // 1 week
	MaxFileSize   int64  `env:"MAXFILESIZE, default=50"`  // 50MB
	UploadDir     string `env:"UPLOADDIR, default=./data/files"`
	HttpTimeoutMs int64  `env:"HTTPTIMEOUTMS, default=1000"` // 1s
}

var ServerConfig Config

func Init() {
	if err := envconfig.Process(context.Background(), &ServerConfig); err != nil {
		log.Fatal(err)
	}

	if util.IsBlank(ServerConfig.SecretKey) {
		ServerConfig.SecretKey = util.GenerateSecureToken(120)
	}

	if !util.IsBlank(ServerConfig.ClientID) && !util.IsBlank(ServerConfig.ClientSecret) && !util.IsBlank(ServerConfig.Issuer) {
		ServerConfig.OidcEnabled = true
	} else {
		log.Println("No OIDC ServerConfig specified.")
	}

	if _, err := os.Stat(ServerConfig.UploadDir); os.IsNotExist(err) {
		os.Mkdir(ServerConfig.UploadDir, os.ModePerm)
	}
}
