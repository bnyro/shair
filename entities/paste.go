package entities

import "gorm.io/gorm"

type Paste struct {
	gorm.Model
	Title   string `json:"title" form:"title"`
	Body    string `json:"body" form:"body"`
	Token   string `json:"token" form:"token"`
	Created int64  `json:"created" form:"created"`
	Expires int64  `json:"expires" form:"expires"`
}
