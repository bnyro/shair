package entities

import "gorm.io/gorm"

type Note struct {
	gorm.Model
	Title  string `json:"title" form:"title"`
	Body   string `json:"body" form:"body"`
	Pinned bool   `json:"pinned" form:"pinned"`
	UserID uint   `json:"userId" form:"userId"`
}
