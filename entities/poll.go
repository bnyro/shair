package entities

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Poll struct {
	gorm.Model
	Title       string         `json:"title" form:"title"`
	Description string         `json:"description" form:"description"`
	Choices     pq.StringArray `json:"choices" gorm:"type:text[]"`
	Votes       pq.Int64Array  `json:"votes" gorm:"type:integer[]"`
	Token       string         `json:"token"`
}
