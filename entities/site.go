package entities

type Site struct {
	Title       string
	Description string
	Image       string
}
