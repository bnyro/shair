package entities

import "gorm.io/gorm"

type Bookmark struct {
	gorm.Model
	Url         string `json:"url" form:"url"`
	Title       string `json:"title" form:"title"`
	Description string `json:"description" form:"description"`
	Thumbnail   string `json:"thumbnail" form:"thumbnail"`
	UserID      uint   `json:"userId" form:"userId"`
	Archived    bool   `json:"archived" form:"archived"`
}
