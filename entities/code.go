package entities

import "gorm.io/gorm"

type CodeSnippet struct {
	gorm.Model
	Title  string `json:"title" form:"title"`
	Raw    string `json:"raw" form:"raw"`
	Lang   string `json:"lang" form:"lang"`
	UserID uint   `json:"userId" form:"userId"`
}
