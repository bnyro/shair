package extractors

import (
	"io"

	"github.com/shair/entities"
	"golang.org/x/net/html"
)

func Extract(resp io.Reader) *entities.Site {
	z := html.NewTokenizer(resp)

	titleFound := false

	hm := new(entities.Site)

	for {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			return hm

		case html.StartTagToken, html.SelfClosingTagToken:
			t := z.Token()

			if t.Data == `body` {
				return hm
			}

			if t.Data == "title" {
				titleFound = true
			}

			if t.Data == "meta" {
				if desc, ok := extractMetaProperty(t, "description"); ok {
					hm.Description = desc
				}

				if ogTitle, ok := extractMetaProperty(t, "og:title"); ok {
					hm.Title = ogTitle
				}

				if ogDesc, ok := extractMetaProperty(t, "og:description"); ok {
					hm.Description = ogDesc
				}

				if ogImage, ok := extractMetaProperty(t, "og:image"); ok {
					hm.Image = ogImage
				}

				if twitterTitle, ok := extractMetaProperty(t, "twitter:title"); ok {
					hm.Title = twitterTitle
				}

				if twitterDesc, ok := extractMetaProperty(t, "twitter:description"); ok {
					hm.Description = twitterDesc
				}

				if twitterImage, ok := extractMetaProperty(t, "twitter:image"); ok {
					hm.Image = twitterImage
				}
			}

		case html.TextToken:
			if titleFound {
				t := z.Token()
				hm.Title = t.Data
				titleFound = false
			}
		}
	}
}

func extractMetaProperty(t html.Token, prop string) (content string, ok bool) {
	for _, attr := range t.Attr {
		if (attr.Key == "property" || attr.Key == "name") && attr.Val == prop {
			ok = true
		}

		if attr.Key == "content" {
			content = attr.Val
		}
	}

	return
}
