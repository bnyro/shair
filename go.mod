module github.com/shair

go 1.21.1

require (
	github.com/boombuler/barcode v1.0.1
	github.com/coreos/go-oidc/v3 v3.11.0
	github.com/dustin/go-humanize v1.0.1
	github.com/fatih/structs v1.1.0
	github.com/glebarez/sqlite v1.11.0
	github.com/go-enry/go-enry/v2 v2.9.1
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/labstack/echo/v4 v4.12.0
	github.com/lib/pq v1.10.9
	github.com/mojocn/base64Captcha v1.3.6
	github.com/pquerna/otp v1.4.0
	golang.org/x/crypto v0.31.0
	golang.org/x/net v0.27.0
	golang.org/x/oauth2 v0.24.0
	golang.org/x/time v0.5.0
	gorm.io/gorm v1.25.7
)

require (
	github.com/dlclark/regexp2 v1.11.0 // indirect
	github.com/go-enry/go-oniguruma v1.2.1 // indirect
	github.com/go-jose/go-jose/v4 v4.0.4 // indirect
	github.com/sethvargo/go-envconfig v1.1.0 // indirect
)

require (
	github.com/alecthomas/chroma/v2 v2.14.0
	github.com/glebarez/go-sqlite v1.21.2 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/image v0.17.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	modernc.org/libc v1.22.5 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.5.0 // indirect
	modernc.org/sqlite v1.23.1 // indirect
)
