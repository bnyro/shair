package util

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func setCookieSecureInternal(c echo.Context, key, value string, maxAge int, path string) {
	cookie := http.Cookie{
		Name:     key,
		Value:    value,
		MaxAge:   maxAge,
		Path:     path,
		Secure:   IsHttps(c),
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	c.SetCookie(&cookie)
}

func SetCookieSecureWithMaxAge(c echo.Context, key, value string, maxAge int) {
	setCookieSecureInternal(c, key, value, maxAge, "/")
}

func SetCookieSecureWithPath(c echo.Context, key, value string, path string) {
	setCookieSecureInternal(c, key, value, 0, path)
}

func SetCookieSecure(c echo.Context, key, value string) {
	setCookieSecureInternal(c, key, value, 0, "/")
}
