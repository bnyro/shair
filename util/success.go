package util

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/labstack/echo/v4"
)

func CreateSuccessResult(c echo.Context, path string, token string) error {
	return createSuccessResultInternal(c, path, token, false, nil)
}

func CreateSuccessResultWithDownloadPossible(c echo.Context, path string, token string) error {
	return createSuccessResultInternal(c, path, token, true, nil)
}

func CreateSuccessResultWithExtra(c echo.Context, path string, token string, extraData map[string]string) error {
	return createSuccessResultInternal(c, path, token, false, extraData)
}

func createSuccessResultInternal(c echo.Context, path string, token string, downloadPossible bool, extraData map[string]string) error {
	visitUrl := GetUrlForPath(c, fmt.Sprintf("/%s/%s", path, token))

	if extraData != nil {
		q := visitUrl.Query()
		for key, value := range extraData {
			if !IsBlank(value) {
				q.Add(key, value)
			}
		}
		visitUrl.RawQuery = q.Encode()
	}

	data := echo.Map{
		"Token":      token,
		"VisitUrl":   visitUrl.String(),
		"EncodedUrl": url.QueryEscape(visitUrl.String()),
	}

	if downloadPossible {
		data["DownloadUrl"] = GetUrlForPath(c, fmt.Sprintf("/files/%s", token)).String()
	}

	return c.Render(http.StatusCreated, "success.html", data)
}
