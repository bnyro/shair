package util

import (
	"bytes"
	_ "embed"
	"fmt"
	"html"
	"regexp"
	"strings"

	"github.com/alecthomas/chroma/v2/quick"
)

//go:embed languages.txt
var languagesString string

var urlRegex, _ = regexp.Compile("(https?:\\/\\/(www\\.)?)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")

func IsBlank(text string) bool {
	return len(strings.TrimSpace(text)) == 0
}

func FirstN(text string, length int) string {
	if len(text) < length {
		return text
	}
	return text[:length]
}

func Urlify(text string) string {
	text = html.EscapeString(text)

	urlified := urlRegex.ReplaceAllStringFunc(text, func(url string) string {
		correctedUrl := url

		if !strings.HasPrefix(url, "http") {
			correctedUrl = "https://" + url
		}

		return fmt.Sprintf("<a href=\"%s\">%s</a>", correctedUrl, url)
	})

	return urlified
}

func CodeHighlight(raw string, language string) string {
	var buff bytes.Buffer

	err := quick.Highlight(&buff, raw, language, "html", "doom-one")
	if err != nil {
		return raw
	}

	return buff.String()
}

func HighlightLanguages() []string {
	// generate with chroma --list | grep aliases | awk -F ': ' '{print $2}' | sed 's/\ /\n/g'
	return strings.Split(languagesString, "\n")
}

func IfBlank(value, defaultValue string) string {
	if IsBlank(value) {
		return defaultValue
	}

	return value
}
