package util

func MakeIntArray(size int) (arr []int) {
	arr = make([]int, size)
	for i := 0; i < size; i++ {
		arr[i] = i
	}
	return
}
